import React, { Component } from "react";

export class FormSubmit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      submit: this.props.submit !== undefined ? this.props.submit : {},
      selectedOptions:
        this.props.selectedOptions !== undefined
          ? this.props.selectedOptions
          : []
    };

    this.addText = this.addText.bind(this);
    this.addImage = this.addImage.bind(this);
    this.send = this.send.bind(this);
  }

  addImage(image) {
    // TODO: write this method
  }

  addText(event) {
    const { submit } = this.state;
    // TODO: write this method

    submit.text = event.target.value;
    this.setState({ submit });
  }

  send() {
    // TODO: write this method
    console.log(this.state);
  }

  render() {
    return (
      <div>
        <header>Please describe the issue</header>
        <textarea onChange={this.addText} />
        <p>Please upload an image</p>
        <input type="file" />
        <div>
          <button onClick={this.send}>Submit</button>
        </div>
      </div>
    );
  }
}

export default FormSubmit;
