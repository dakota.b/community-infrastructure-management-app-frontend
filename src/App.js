import React from "react";
import Webfront from "./Webfront";

function App() {
  return (
    <div className="App">
      <Webfront branches="api/branches" options="api/options" />
    </div>
  );
}

export default App;
